package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


// "@SpringBootApplication" is an example of "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
// These are used to manage and configure the behavior of the framework.
// Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities
// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.
@SpringBootApplication

@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")


	public String hello(@RequestParam(value = "name",
	defaultValue = "World") String name){
	return String.format("Hello %s!", name);

		}

	@GetMapping("/hi")

	public String hi(@RequestParam(value = "name",
			defaultValue = "user") String name){
		return String.format("hi %s!", name);

	}

	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value = "name", defaultValue = "user") String name,
						  @RequestParam(value = "age", defaultValue = "immortal") int age) {
		return String.format("Hello %s! Your age is %d.", name, age);
	}

}